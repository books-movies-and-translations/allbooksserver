@file:OptIn(ExperimentalPathApi::class)

import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.deleteRecursively

plugins {
    kotlin("jvm") version "1.9.22"
    kotlin("plugin.serialization") version "1.9.22"
    id("org.openapi.generator") version "7.4.0"
    id("io.ktor.plugin") version "2.3.7"
    application
}

group = "de.jk"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.ktor:ktor-server-core")
    implementation("io.ktor:ktor-server-netty")
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm")
    implementation("io.ktor:ktor-server-content-negotiation")
    implementation("io.ktor:ktor-server-cors")
    implementation("org.slf4j:slf4j-simple:1.7.32")

    implementation("io.github.jan-tennert.supabase:postgrest-kt:2.2.3")
    implementation("io.ktor:ktor-client-java:2.2.3")


    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(11)
}

val generatedSourcesFolder = "$rootDir/src/main/kotlin/generated"
val generationOutputDir = "$buildDir/generated"

openApiGenerate {
    //make sure that the build directory exists
    Files.createDirectories(Paths.get("$buildDir"))

    generatorName.set("kotlin")
    val yamlUrl = "https://gitlab.com/books-movies-and-translations/allbooksmodel/-/raw/main/v0/AllBooks.yaml"
    download(yamlUrl, "$buildDir/AllBooks.yaml")

    inputSpec.set("$buildDir/AllBooks.yaml")
    outputDir.set(generationOutputDir)

    packageName.set("generated")

    configOptions.set(
        mapOf(
            "dateLibrary" to "kotlinx-datetime",
            "library" to "multiplatform",
        )
    )
}

fun download(url: String, path: String) {
    val destFile = File(path)
    ant.invokeMethod("get", mapOf("src" to url, "dest" to destFile))
}

application {
    mainClass.set("MainKt")
}

tasks.compileKotlin {
    dependsOn(tasks.openApiGenerate)
}

tasks.register("deleteGeneratedDirectories") {
    val generatedDirectories = listOf(generatedSourcesFolder)

    generatedDirectories.forEach {
        Paths.get(it).deleteRecursively()
    }
}

tasks.named("openApiGenerate").configure {
    finalizedBy("copyGeneratedFiles")
}

tasks.register("copyGeneratedFiles") {
    doLast {
        val sourceDir = "$generationOutputDir/src/main/kotlin/generated/models"
        val targetDir = "$generatedSourcesFolder/models"

        // Ensure the target directory exists
        Files.createDirectories(Paths.get(targetDir))

        // Copy each file from the source to the target directory
        Files.list(Paths.get(sourceDir))
            .forEach { sourcePath ->
                val targetPath = Paths.get(targetDir, sourcePath.fileName.toString())
                Files.copy(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING)
            }
    }
}

tasks.named("copyGeneratedFiles").configure {
    dependsOn("deleteGeneratedDirectories")
}

tasks.named("clean").configure {
    finalizedBy("deleteGeneratedDirectories")
}
