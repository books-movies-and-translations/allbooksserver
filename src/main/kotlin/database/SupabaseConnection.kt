package database

import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.postgrest.Postgrest

private const val public_anon_key =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InRrcm5oa2huYWpvdWx2bGFkaGl1Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDUyNDc1MTAsImV4cCI6MjAyMDgyMzUxMH0.Ex-JFw9SK1Alo2_EAnEXR8AnxHGYEihO7qX_7igzV4w"

val supabase = createSupabaseClient(
    supabaseUrl = "https://tkrnhkhnajoulvladhiu.supabase.co",
    supabaseKey = public_anon_key
) {
    install(Postgrest)
}
