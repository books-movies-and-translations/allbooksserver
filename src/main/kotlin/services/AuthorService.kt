package services

import database.supabase
import generated.models.Author
import io.github.jan.supabase.postgrest.from

class AuthorService {
    suspend fun getAllAuthors(): List<Author> {
        return supabase.from("Authors").select().decodeList<Author>()
    }

    suspend fun addAuthor(author: Author) {
        supabase.from("Authors").insert(author)
    }

}