package services

import generated.models.Book

class BookService {
    private val books: MutableList<Book> = mutableListOf()

    fun getAllBooks(): List<Book> {
        return books
    }

    fun addBook(book: Book) {
        this.books.add(book)
    }
}