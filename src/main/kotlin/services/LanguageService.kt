package services

import database.supabase
import generated.models.Language
import io.github.jan.supabase.postgrest.from

class LanguageService {

    suspend fun getAllLanguages(): List<Language> {
        return supabase.from("Languages").select().decodeList<Language>()
    }

}