@file:Suppress("SpellCheckingInspection")

import generated.models.Author
import generated.models.Book
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.json.Json
import services.AuthorService
import services.BookService
import services.LanguageService

fun main() {
    val bookService = BookService()
    val languageService = LanguageService()
    val authorService = AuthorService()

    embeddedServer(Netty, port = 8080) {
        install(CORS) {
            anyHost()
            allowHeader(HttpHeaders.ContentType)
        }
        server(bookService, languageService, authorService)
    }.start(wait = true)
}

private fun Application.server(
    bookService: BookService,
    languageService: LanguageService,
    authorService: AuthorService
) {
    module()
    routing(bookService, languageService, authorService)
}

private fun Application.routing(
    bookService: BookService,
    languageService: LanguageService,
    authorService: AuthorService
) {
    routing {
        bookRouting(bookService)
        languageRouting(languageService)
        authorRouting(authorService)
    }
}

private fun Routing.authorRouting(authorService: AuthorService) {
    get("/authors") {
        call.respond(authorService.getAllAuthors())
    }
    post("/authors/add") {
        val author = call.receive<Author>()
        authorService.addAuthor(author)
        call.respondText("Author created successfully", status = HttpStatusCode.Created)
    }
}

private fun Routing.languageRouting(languageService: LanguageService) {
    get("/languages") {
        call.respond(languageService.getAllLanguages())
    }
}

private fun Routing.bookRouting(bookService: BookService) {
    get("/books") {
        call.respond(bookService.getAllBooks())
    }
    post("/books/add") {
        val book = call.receive<Book>()
        bookService.addBook(book)
        call.respondText("Book stored successfully", status = HttpStatusCode.Created)
    }
}

private fun Application.module() {
    install(ContentNegotiation) {
        json(Json {
            prettyPrint = true
            isLenient = true
        })
    }
}
